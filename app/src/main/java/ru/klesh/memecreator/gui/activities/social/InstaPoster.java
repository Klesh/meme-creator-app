package ru.klesh.memecreator.gui.activities.social;


import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import ru.klesh.memecreator.R;

public class InstaPoster extends Activity {

    public static String TAG_PHOTO_FILE = "photo";
    public static String TAG_COMMENT = "comment";

    private String photoFile = null;
    private String comment = "";
    private final String INSTAGRAM_PACKAGE = "com.instagram.android";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        if (intent.hasExtra(TAG_PHOTO_FILE)) {
            this.photoFile = getIntent().getStringExtra(TAG_PHOTO_FILE);
        } else {
            Toast.makeText(getBaseContext(), "Photo file for share not found", Toast.LENGTH_LONG).show();
            finish();
        }

        if (intent.hasExtra(TAG_COMMENT)) {
            this.comment = getIntent().getStringExtra(TAG_COMMENT);
        }

        if (isIntagramAppExists()) {
            String message = comment.length() > 0 ? comment + "\n--\n" : "";
            message += getResources().getString(R.string.share_post_scribe);

            shareToInstagram(Uri.fromFile(new File(photoFile)), message);
            finish();
        } else {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + INSTAGRAM_PACKAGE)));
            } catch (android.content.ActivityNotFoundException ignored) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + INSTAGRAM_PACKAGE)));
            }
            finish();
        }
    }

    private void shareToInstagram(Uri imageUri, String message) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("image/*");
        i.putExtra(Intent.EXTRA_STREAM, imageUri);
        i.putExtra(Intent.EXTRA_TEXT, message);
        i.setPackage(INSTAGRAM_PACKAGE);
        this.startActivityForResult(i, 0);
    }

    public boolean isIntagramAppExists() {
        List<ApplicationInfo> packages;
        PackageManager pm;

        pm = getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo.packageName.equals(INSTAGRAM_PACKAGE))
                return true;
        }
        return false;
    }
}
