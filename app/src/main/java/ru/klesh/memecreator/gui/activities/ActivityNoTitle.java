package ru.klesh.memecreator.gui.activities;

import android.os.Build;
import android.os.Bundle;
import android.view.View;

public abstract class ActivityNoTitle extends ActivityBase {

    private int visibleMask = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        visibleMask = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_FULLSCREEN;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            visibleMask |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        this.hideActionBar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.hideActionBar();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            View decorView = getWindow().getDecorView();
            int mask = decorView.getSystemUiVisibility();
            if (mask != visibleMask) {
                decorView.setSystemUiVisibility(visibleMask);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.hideActionBar();
    }

    public void hideActionBar() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(visibleMask);
    }
}
