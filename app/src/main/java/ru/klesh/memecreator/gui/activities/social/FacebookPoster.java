package ru.klesh.memecreator.gui.activities.social;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.widget.ShareDialog;

import ru.klesh.memecreator.R;

public class FacebookPoster extends Activity {

    public static String TAG_PHOTO_FILE = "photo";

    private String photoFile = null;
    private CallbackManager callbackManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_share);

        Intent intent = getIntent();

        if (intent.hasExtra(TAG_PHOTO_FILE)) {
            this.photoFile = getIntent().getStringExtra(TAG_PHOTO_FILE);
        } else {
            Toast.makeText(getBaseContext(), "Photo file for share not found", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        makePost();
    }

    public Bitmap getPhoto() {
        return BitmapFactory.decodeFile(photoFile);
    }


    private void makePost() {
        SharePhoto sharePhoto = new SharePhoto.Builder()
                .setBitmap(getPhoto())
                .build();
        ShareContent shareContent = new ShareMediaContent.Builder()
                .addMedium(sharePhoto)
                .build();

        callbackManager = CallbackManager.Factory.create();

        ShareDialog shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(getBaseContext(), R.string.social_share_success_text, Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getBaseContext(), R.string.social_user_cancel_text, Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getBaseContext(), R.string.social_share_fail_text, Toast.LENGTH_SHORT).show();
                Toast.makeText(getBaseContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                finish();
            }
        });

        shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
