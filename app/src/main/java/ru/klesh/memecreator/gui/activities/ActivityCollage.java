package ru.klesh.memecreator.gui.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;

import ru.klesh.memecreator.R;
import ru.klesh.memecreator.common.SoundManager;
import ru.klesh.memecreator.common.lib.Sounds;
import ru.klesh.memecreator.common.lib.utils.BitmapUtils;
import ru.klesh.memecreator.common.meme.Meme;
import ru.klesh.memecreator.common.meme.MemeManager;
import ru.klesh.memecreator.gui.SmallSoundOverlayConfig;
import ru.klesh.memecreator.gui.SoundOverlayConfig;
import ru.klesh.memecreator.gui.activities.social.FacebookPoster;
import ru.klesh.memecreator.gui.activities.social.InstaPoster;
import ru.klesh.memecreator.gui.activities.social.TwitterPoster;
import ru.klesh.memecreator.gui.activities.social.VKPoster;

public class ActivityCollage extends ActivityApp {

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_CROP_PICTURE = 2;
    private static final int REQUEST_CROP_MEME = 3;
    private static final int REQUEST_SOCIAL_SHARE = 4;

    public static final String TAG_CURRENT_MEME_INDEX = "current_meme_idx";
    public static final String TAG_SAVED_COLLAGE = "saved_collage";

    private File currPhotoFile;
    private File currMemeFile;
    private File collageFile;

    private ImageView collageView;
    private EditText commentEdit;
    private LinearLayout memeLayout;

    private int currMemeIndex;
    private Meme[] memes;

    private Bitmap collageBitmap = null;
    private boolean isVerticalCollage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.collageView = (ImageView) findViewById(R.id.collage_image);
        this.commentEdit = (EditText) findViewById(R.id.share_comment_edit);
        this.memeLayout = (LinearLayout) findViewById(R.id.meme_collage);

        this.memes = MemeManager.INSTANCE.getMemes();

        this.registerOnClickListener(R.id.share_facebook_btn);
        this.registerOnClickListener(R.id.share_instagram_btn);
        this.registerOnClickListener(R.id.share_twitter_btn);
        this.registerOnClickListener(R.id.share_vk_btn);

        if (savedInstanceState != null) {
            this.currMemeIndex = savedInstanceState.getInt(TAG_CURRENT_MEME_INDEX, -1);

            String collagePath = savedInstanceState.getString(TAG_SAVED_COLLAGE, null);
            if (collagePath != null) {
                this.collageFile = new File(collagePath);
                this.collageBitmap = BitmapFactory.decodeFile(collagePath);
                this.updateCollageView();
            }

            return;
        }

        Intent intent = getIntent();
        if (intent.hasExtra(TAG_CURRENT_MEME_INDEX)) {
            this.currMemeIndex = intent.getIntExtra(TAG_CURRENT_MEME_INDEX, 0);
        } else {
            Toast.makeText(context, "Intent extra meme id data is missing", Toast.LENGTH_SHORT).show();
            finish();
        }

        this.dispatchTakePictureIntent();
    }

    @Override
    public Object[] getCheckableViews() {
        return new Object[]{collageView, commentEdit, memeLayout};
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(TAG_CURRENT_MEME_INDEX, currMemeIndex);

        if (collageFile != null) {
            outState.putString(TAG_SAVED_COLLAGE, collageFile.getAbsolutePath());
        }
    }

    @Override
    public int getLayoutResId() {
        return R.layout.activity_collage;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                this.currPhotoFile = createImageFile("meme");
            } catch (IOException ex) {
                Toast.makeText(context, "Can't create temp image file: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                ex.printStackTrace();
            }

            if (currPhotoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(currPhotoFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File getStorageDir() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
    }

    private File createImageFile(String name) throws IOException {
        return File.createTempFile(name, ".jpg", getStorageDir());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        BitmapFactory.Options opts;
        CropImage.ActivityResult result;

        switch (requestCode) {
            case REQUEST_IMAGE_CAPTURE:
                if (resultCode != RESULT_OK) {
                    finish();
                    return;
                }

                Toast.makeText(context, R.string.photo_crop_zone_select_text, Toast.LENGTH_SHORT).show();

                Intent intent = CropImage.activity(Uri.fromFile(currPhotoFile))
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setActivityTitle(getString(R.string.crop_action_text))
                        .getIntent(this);
                startActivityForResult(intent, REQUEST_CROP_PICTURE);
                break;
            case REQUEST_CROP_PICTURE:
                if (resultCode != RESULT_OK) {
                    finish();
                    return;
                }

                result = CropImage.getActivityResult(data);

                this.removePhotoFile(currPhotoFile);
                this.currPhotoFile = new File(result.getUri().getPath());

                opts = BitmapUtils.decodeBitmapBoundsOnly(this.currPhotoFile.getAbsolutePath());

                int w = opts.outWidth;
                int h = opts.outHeight;

                this.isVerticalCollage = checkVerticalOrientation(w, h);

                Toast.makeText(context, R.string.meme_crop_zone_select_text, Toast.LENGTH_SHORT).show();

                intent = CropImage.activity(Uri.fromFile(memes[currMemeIndex].getImageFile()))
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(w, h)
                        .setFixAspectRatio(true)
                        .getIntent(this);

                startActivityForResult(intent, REQUEST_CROP_MEME);


                break;
            case REQUEST_CROP_MEME:
                if (resultCode != RESULT_OK) {
                    finish();
                    return;
                }

                result = CropImage.getActivityResult(data);

                this.currMemeFile = new File(result.getUri().getPath());
                this.collageBitmap = this.buildCollageBitmap();
                this.collageFile = this.saveCollageBitmap();

                this.removePhotoFile(this.currPhotoFile);
                this.removePhotoFile(this.currMemeFile);
                this.updateCollageView();
                break;
            case REQUEST_SOCIAL_SHARE:
                break;
            default:
                finish();
                break;
        }
    }

    @Override
    public SoundOverlayConfig getSoundOverlayConfig() {
        return new SmallSoundOverlayConfig();
    }

    private boolean checkVerticalOrientation(int photoW, int photoH) {
        return photoW > photoH;
    }

    private void updateCollageView() {
        this.collageView.setImageBitmap(this.collageBitmap);
    }

    private Bitmap buildCollageBitmap() {
        int size = resources.getInteger(R.integer.collage_meme_image_thumb_size);

        Bitmap phtBmp = BitmapUtils.decodeSampledBitmapFromFile(this.currPhotoFile.getAbsolutePath(), size, size);
        Bitmap memBmp = BitmapUtils.decodeSampledBitmapFromFile(this.currMemeFile.getAbsolutePath(), size, size);

        int w = phtBmp.getWidth();
        int h = phtBmp.getHeight();

        Bitmap plate;

        if (this.isVerticalCollage) {
            plate = Bitmap.createBitmap(w, h + h, Bitmap.Config.RGB_565);
        } else {
            plate = Bitmap.createBitmap(w + w, h, Bitmap.Config.RGB_565);
        }

        memBmp = Bitmap.createScaledBitmap(memBmp, w, h, true);

        Canvas canvas = new Canvas(plate);
        Paint paint = new Paint();
        paint.setColor(0xFFFFFFFF);
        canvas.drawRect(0, 0, plate.getWidth(), plate.getHeight(), paint);

        if (this.isVerticalCollage) {
            canvas.drawBitmap(phtBmp, 0, 0, paint);
            canvas.drawBitmap(memBmp, 0, h, paint);
        } else {
            canvas.drawBitmap(phtBmp, 0, 0, paint);
            canvas.drawBitmap(memBmp, w, 0, paint);
        }

        return plate;
    }

    private File saveCollageBitmap() {
        try {
            File f = new File(getStorageDir(), "collage_" + System.currentTimeMillis() % 1000 + ".jpg");

            if (f.exists()) {
                if (!f.delete()) {
                    throw new IOException("Can't delete exists file");
                }
            }

            if (!f.createNewFile()) {
                throw new IOException("Can't create new collage file");
            }

            BitmapUtils.saveBitmapToFile(f, this.collageBitmap);

            return f;
        } catch (IOException e) {
            Toast.makeText(context, "Fail: " + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (currMemeIndex < 0) {
            currMemeIndex = 0;
        }

        if (currMemeIndex > memes.length) {
            currMemeIndex = memes.length - 1;
        }

        boolean sound = true;

        switch (v.getId()) {
            case R.id.share_facebook_btn:
                if (collageFile != null) {
                    Intent fbPoster = new Intent(this, FacebookPoster.class);
                    fbPoster.putExtra(FacebookPoster.TAG_PHOTO_FILE, collageFile.getAbsolutePath());
                    startActivityForResult(fbPoster, REQUEST_SOCIAL_SHARE);
                }
                break;
            case R.id.share_instagram_btn:
                if (collageFile != null) {
                    Intent instaPoster = new Intent(this, InstaPoster.class);
                    instaPoster.putExtra(InstaPoster.TAG_PHOTO_FILE, collageFile.getAbsolutePath());
                    startActivityForResult(instaPoster, REQUEST_SOCIAL_SHARE);
                }
                break;
            case R.id.share_twitter_btn:
                if (collageFile != null) {
                    Intent twitterPoster = new Intent(this, TwitterPoster.class);
                    twitterPoster.putExtra(TwitterPoster.TAG_PHOTO_FILE, collageFile.getAbsolutePath());
                    twitterPoster.putExtra(TwitterPoster.TAG_COMMENT, commentEdit.getText().toString());
                    startActivityForResult(twitterPoster, REQUEST_SOCIAL_SHARE);
                }
                break;
            case R.id.share_vk_btn:
                if (collageFile != null) {
                    Intent vkPoster = new Intent(this, VKPoster.class);
                    vkPoster.putExtra(VKPoster.TAG_PHOTO_FILE, collageFile.getAbsolutePath());
                    vkPoster.putExtra(VKPoster.TAG_COMMENT, commentEdit.getText().toString());
                    startActivityForResult(vkPoster, REQUEST_SOCIAL_SHARE);
                }
                break;
            default:
                sound = false;
                break;
        }

        if (sound) {
            SoundManager.INSTANCE.playRandomly(Sounds.BUTTON_TAP);
        }
    }

    private void removePhotoFile(File file) {
        if (file != null && file.exists() && !file.delete()) {
            Toast.makeText(context, "Can't remove file: " + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        }
    }
}
