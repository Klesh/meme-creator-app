package ru.klesh.memecreator.gui;


import ru.klesh.memecreator.R;

public class SoundOverlayConfig {

    public final int soundButtonResId;

    public final int drawableOnResId;

    public final int drawableOffResId;

    public SoundOverlayConfig(int soundButtonResId, int drawableOnResId, int drawableOffResId) {
        this.soundButtonResId = soundButtonResId;
        this.drawableOnResId = drawableOnResId;
        this.drawableOffResId = drawableOffResId;
    }

    public SoundOverlayConfig() {
        this.soundButtonResId = R.id.toggle_sound;
        this.drawableOnResId = R.drawable.selector_sound_on;
        this.drawableOffResId = R.drawable.selector_sound_off;
    }
}
