package ru.klesh.memecreator.gui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import ru.klesh.memecreator.R;
import ru.klesh.memecreator.common.lib.utils.ImageViewFileLoadTask;
import ru.klesh.memecreator.common.meme.Meme;
import ru.klesh.memecreator.common.meme.MemeManager;
import ru.klesh.memecreator.gui.SmallSoundOverlayConfig;
import ru.klesh.memecreator.gui.SoundOverlayConfig;

public class ActivityCollectionFull extends ActivityApp {

    public static final String TAG_MEME_ID = "meme_id";
    private ImageView image;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_collection_full;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        if (intent.hasExtra(TAG_MEME_ID)) {
            Meme meme = MemeManager.INSTANCE.getMemes()[intent.getIntExtra(TAG_MEME_ID, 0)];
            image = (ImageView) findViewById(R.id.meme_image);

            if (image != null) {
                ImageViewFileLoadTask task = new ImageViewFileLoadTask(image, meme.getImageFile().getAbsolutePath());
                int size = resources.getInteger(R.integer.collection_full_meme_image_thumb_size);
                task.execute(size, size);
            }

            TextView text = (TextView) findViewById(R.id.meme_name_text);
            if (text != null) {
                text.setText(meme.getName());
            }

            text = (TextView) findViewById(R.id.meme_desc_text);
            if (text != null) {
                text.setText(meme.getDesc());
            }
        }
    }

    @Override
    public Object[] getCheckableViews() {
        return new Object[]{image};
    }

    @Override
    public SoundOverlayConfig getSoundOverlayConfig() {
        return new SmallSoundOverlayConfig();
    }
}
