package ru.klesh.memecreator.gui;

import ru.klesh.memecreator.R;

public class SmallSoundOverlayConfig extends SoundOverlayConfig {

    public SmallSoundOverlayConfig() {
        super(R.id.small_toggle_sound, R.drawable.small_sound_on, R.drawable.small_sound_off);
    }
}
