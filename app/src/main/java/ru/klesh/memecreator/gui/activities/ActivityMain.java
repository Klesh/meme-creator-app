package ru.klesh.memecreator.gui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import ru.klesh.memecreator.R;
import ru.klesh.memecreator.common.SoundManager;
import ru.klesh.memecreator.common.lib.Sounds;
import ru.klesh.memecreator.common.lib.utils.BitmapUtils;
import ru.klesh.memecreator.gui.SoundOverlayConfig;

public class ActivityMain extends ActivityApp {

    @Override
    public int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected int getBackgroundDrawableId() {
        return R.drawable.menu_bg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.registerOnClickListener(R.id.go_faq);
        this.registerOnClickListener(R.id.go_play);
        //this.registerOnClickListener(R.id.go_collection);
        this.registerOnClickListener(R.id.toggle_sound);

        ImageView logo = (ImageView) findViewById(R.id.main_logo_image);
        if (logo != null) {
            int size = resources.getInteger(R.integer.logo_image_thumb_size);
            logo.setImageBitmap(BitmapUtils.decodeSampledBitmapResource(resources, R.drawable.logo, size, size));
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Intent intent = null;

        switch (v.getId()) {
            case R.id.go_faq:
                intent = new Intent(this, ActivityFaq.class);
                break;
            case R.id.go_play:
                intent = new Intent(this, ActivityPlay.class);
                break;
//            case R.id.go_collection:
//                intent = new Intent(this, ActivityCollection.class);
//                break;
        }

        if (intent != null) {
            SoundManager.INSTANCE.playRandomly(Sounds.BUTTON_TAP);
            this.startActivity(intent);
        }
    }

    @Override
    public SoundOverlayConfig getSoundOverlayConfig() {
        return new SoundOverlayConfig();
    }

}
