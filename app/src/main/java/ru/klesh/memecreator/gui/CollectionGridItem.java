package ru.klesh.memecreator.gui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.klesh.memecreator.R;
import ru.klesh.memecreator.common.lib.utils.BitmapUtils;
import ru.klesh.memecreator.common.lib.utils.ViewUtils;
import ru.klesh.memecreator.common.meme.Meme;

public class CollectionGridItem {

    private View.OnClickListener onClickListener;
    private Meme meme;

    public CollectionGridItem(Meme meme, View.OnClickListener clickListener) {
        this.onClickListener = clickListener;
        this.meme = meme;
    }

    public static class Adapter extends BaseAdapter {

        private Context context;
        private final List<CollectionGridItem> items;
        private Map<Integer, View> views = new HashMap<>();
        private LayoutInflater inflater;

        public Adapter(Context context) {
            this.context = context;
            this.items = new ArrayList<>();
            this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void addItem(CollectionGridItem view) {
            items.add(view);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = views.get(position);

            if (view == null) {
                CollectionGridItem item = items.get(position);
                view = inflater.inflate(R.layout.collection_grid_item, parent, false);
                ImageView image = (ImageView) view.findViewById(R.id.collection_grid_item_image);
                TextView text = (TextView) view.findViewById(R.id.collection_grid_item_text);
                Resources resources = view.getResources();

                text.setText(item.meme.getName());

                int size = resources.getInteger(R.integer.collection_grid_item_thumb_size);

                Bitmap bitmap = BitmapUtils.decodeSampledBitmapFromFile(item.meme.getImageFile().getAbsolutePath(), size, size);

                if (bitmap != null) {
                    int w = bitmap.getWidth();
                    int h = bitmap.getHeight();
                    int lessSide = w >= h ? h : w;
                    int startX = Math.max((w - h) / 2, 0);
                    int startY = Math.max((h - w) / 2, 0);

                    bitmap = Bitmap.createBitmap(bitmap, startX, startY, startX + lessSide, startY + lessSide);

                    if (bitmap != null) {
                        bitmap = Bitmap.createScaledBitmap(bitmap, size, size, false);
                    }
                }

                if (bitmap == null) {
                    bitmap = BitmapFactory.decodeResource(resources, R.drawable.fill);
                }

                int radius = resources.getInteger(R.integer.collection_grid_item_corner_radius);
                image.setImageBitmap(ViewUtils.getRoundedCornerBitmap(bitmap, radius));
                image.setTag(R.integer.grid_item_image_view_tag_id_meme_id, position);

                if (item.onClickListener != null) {
                    image.setOnClickListener(item.onClickListener);
                }

                views.put(position, view);
            }

            return view;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

    }
}
