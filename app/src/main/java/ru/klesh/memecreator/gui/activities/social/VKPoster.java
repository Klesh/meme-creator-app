package ru.klesh.memecreator.gui.activities.social;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Toast;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKAttachments;
import com.vk.sdk.api.model.VKPhotoArray;
import com.vk.sdk.api.model.VKWallPostResult;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;

import ru.klesh.memecreator.R;

public class VKPoster extends Activity {

    public static String TAG_PHOTO_FILE = "photo";
    public static String TAG_COMMENT = "comment";

    private static boolean isLogin = false;

    private String photoFile = null;
    private String comment = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_share);

        Intent intent = getIntent();

        if (intent.hasExtra(TAG_PHOTO_FILE)) {
            this.photoFile = getIntent().getStringExtra(TAG_PHOTO_FILE);
        } else {
            Toast.makeText(getBaseContext(), "Photo file for share not found", Toast.LENGTH_LONG).show();
            finish();
        }

        if (intent.hasExtra(TAG_COMMENT)) {
            this.comment = getIntent().getStringExtra(TAG_COMMENT);
        }

        if (!isLogin) {
            VKSdk.login(this, VKScope.WALL, VKScope.PHOTOS);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isLogin) {
            makePost();
        }
    }

    public Bitmap getPhoto() {
        return BitmapFactory.decodeFile(photoFile);
    }

    private void makePost() {
        final Bitmap photo = getPhoto();
        if (photo != null) {
            VKRequest request = VKApi.uploadWallPhotoRequest(new VKUploadImage(photo, VKImageParameters.jpgImage(0.9f)), 0, 0);

            request.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    photo.recycle();
                    VKApiPhoto photoModel = ((VKPhotoArray) response.parsedModel).get(0);

                    VKAttachments attachments = new VKAttachments(photoModel);
                    String message = comment.length() > 0 ? comment + "\n--\n" : "";
                    message += getResources().getString(R.string.share_post_scribe);

                    makePost(attachments, message);
                }

                @Override
                public void onError(VKError error) {
                    photo.recycle();
                    Toast.makeText(getBaseContext(), R.string.social_upload_fail_text, Toast.LENGTH_LONG).show();
                    finish();
                }
            });
        }
    }

    private void makePost(VKAttachments attachments, String message) {
        VKRequest post = VKApi.wall().post(VKParameters.from(
                VKApiConst.ATTACHMENTS, attachments,
                VKApiConst.MESSAGE, message));

        post.setModelClass(VKWallPostResult.class);
        post.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {

                Toast.makeText(getBaseContext(), R.string.social_share_success_text, Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onError(VKError error) {
                Toast.makeText(getBaseContext(), R.string.social_share_fail_text, Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                isLogin = true;
                makePost();
            }

            @Override
            public void onError(VKError error) {
                isLogin = false;
                Toast.makeText(getBaseContext(), R.string.social_auth_fail_text, Toast.LENGTH_LONG).show();
                finish();
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
