package ru.klesh.memecreator.gui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Random;

import ru.klesh.memecreator.R;
import ru.klesh.memecreator.common.SoundManager;
import ru.klesh.memecreator.common.lib.Sounds;
import ru.klesh.memecreator.common.lib.utils.ImageViewFileLoadTask;
import ru.klesh.memecreator.common.meme.Meme;
import ru.klesh.memecreator.common.meme.MemeManager;
import ru.klesh.memecreator.gui.SoundOverlayConfig;

public class ActivityPlay extends ActivityApp {

    public static final String TAG_MEME_VIEW_HISTORY = "view_history";

    private Meme[] memes;
    private ImageView memeImage;
    private ArrayList<Integer> viewHistory;
    private Random rand = new Random();

    @Override
    public int getLayoutResId() {
        return R.layout.activity_play;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.memes = MemeManager.INSTANCE.getMemes();
        this.memeImage = (ImageView) findViewById(R.id.meme_image);

        this.registerOnClickListener(R.id.play_next_button);
        this.registerOnClickListener(R.id.play_shot_button);
        this.registerOnClickListener(R.id.play_prev_button);
        this.registerOnClickListener(R.id.go_meme_desc);

        if (savedInstanceState != null) {
            viewHistory = savedInstanceState.getIntegerArrayList(TAG_MEME_VIEW_HISTORY);
        }

        if (viewHistory == null) {
            viewHistory = new ArrayList<>();
            viewHistory.add(rand.nextInt(memes.length));
        }

        this.displayLastMeme();
    }

    @Override
    public Object[] getCheckableViews() {
        return new Object[]{memeImage};
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntegerArrayList(TAG_MEME_VIEW_HISTORY, viewHistory);
    }

    @Override
    public SoundOverlayConfig getSoundOverlayConfig() {
        return new SoundOverlayConfig();
    }

    private void displayLastMeme() {
        Meme meme = memes[this.getCurrentMemeIndex()];
        ImageViewFileLoadTask task = new ImageViewFileLoadTask(memeImage, meme.getImageFile().getAbsolutePath());
        int size = resources.getInteger(R.integer.play_meme_image_thumb_size);
        task.execute(size, size);
    }

    private int getCurrentMemeIndex() {
        return viewHistory.get(viewHistory.size() - 1);
    }

    private int randNewMemeIndex() {
        int ci = getCurrentMemeIndex();
        int nci = ci;

        while (ci == nci) {
            nci = rand.nextInt(memes.length);
        }

        return nci;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        Intent intent;
        boolean sound = true;

        switch (v.getId()) {
            case R.id.play_next_button:
                this.viewHistory.add(randNewMemeIndex());
                this.displayLastMeme();
                break;
            case R.id.play_prev_button:
                if (this.viewHistory.size() > 1) {
                    this.viewHistory.remove(viewHistory.size() - 1);
                }
                this.displayLastMeme();
                break;
            case R.id.play_shot_button:
                intent = new Intent(this, ActivityCollage.class);
                intent.putExtra(ActivityCollage.TAG_CURRENT_MEME_INDEX, this.getCurrentMemeIndex());
                this.startActivity(intent);
                break;
            case R.id.go_meme_desc:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(memes[getCurrentMemeIndex()].getDesc())
                        .setTitle(memes[getCurrentMemeIndex()].getName())
                        .setPositiveButton(R.string.close_text, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                builder.create().show();
                break;
            default:
                sound = false;
                break;
        }

        if (sound) {
            SoundManager.INSTANCE.playRandomly(Sounds.BUTTON_TAP);
        }
    }
}
