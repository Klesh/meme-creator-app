package ru.klesh.memecreator.gui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;

import ru.klesh.memecreator.R;
import ru.klesh.memecreator.common.SoundManager;
import ru.klesh.memecreator.common.lib.Sounds;
import ru.klesh.memecreator.common.meme.Meme;
import ru.klesh.memecreator.common.meme.MemeManager;
import ru.klesh.memecreator.gui.CollectionGridItem;
import ru.klesh.memecreator.gui.SmallSoundOverlayConfig;
import ru.klesh.memecreator.gui.SoundOverlayConfig;

public class ActivityCollection extends ActivityApp {

    private GridView gridView;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_collection;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addItems();
    }

    private void addItems() {
        gridView = (GridView) findViewById(R.id.collection_meme_grid);

        if (gridView != null) {
            CollectionGridItem.Adapter adapter = new CollectionGridItem.Adapter(this);

            for (Meme meme : MemeManager.INSTANCE.getMemes()) {
                adapter.addItem(new CollectionGridItem(meme, this));
            }

            gridView.setAdapter(adapter);
        }
    }

    @Override
    public Object[] getCheckableViews() {
        return new Object[]{gridView};
    }

    @Override
    public SoundOverlayConfig getSoundOverlayConfig() {
        return new SmallSoundOverlayConfig();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.collection_grid_item_image:
                SoundManager.INSTANCE.playRandomly(Sounds.BUTTON_TAP);
                Integer id = (Integer) v.getTag(R.integer.grid_item_image_view_tag_id_meme_id);
                if (id != null) {
                    Intent intent = new Intent(this, ActivityCollectionFull.class);
                    intent.putExtra(ActivityCollectionFull.TAG_MEME_ID, id);
                    this.startActivity(intent);
                }
                break;
        }
    }
}
