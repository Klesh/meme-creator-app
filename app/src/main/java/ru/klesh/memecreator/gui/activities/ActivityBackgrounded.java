package ru.klesh.memecreator.gui.activities;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Bundle;
import android.widget.ImageView;

import rapid.decoder.BitmapDecoder;
import ru.klesh.memecreator.R;
import ru.klesh.memecreator.common.BitmapCache;

public abstract class ActivityBackgrounded extends ActivityNoTitle {

    public static final String CACHE_BG_KEY = "bg";
    protected ImageView backgroundImageView;

    private static ImageView lastBgImageView = null;
    private static int lastBgDrawableId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.backgroundImageView = (ImageView) findViewById(R.id.global_bg);
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean reloadBg = lastBgDrawableId != getBackgroundDrawableId();
        if (reloadBg) {
            if (lastBgImageView != null) {
                lastBgImageView.setImageBitmap(null);
            }
            BitmapCache.INSTANCE.putBitmap(CACHE_BG_KEY, null);
        }

        this.setBackground(backgroundImageView);
        lastBgImageView = this.backgroundImageView;
        lastBgDrawableId = getBackgroundDrawableId();
    }

    @Override
    public Object[] getCheckableViews() {
        return new Object[]{backgroundImageView};
    }

    protected Rect getBackgroundBounds() {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, getBackgroundDrawableId(), opts);

        int boundW = Math.abs(Math.min(displayWidth - opts.outWidth, 0) / 2);
        int boundH = Math.abs(Math.min(displayHeight - opts.outHeight, 0) / 2);

        int right = Math.min(displayWidth, opts.outWidth);
        int bottom = Math.min(displayHeight, opts.outHeight);

        return new Rect(boundW, boundH, boundW + right, boundH + bottom);
    }

    protected void setBackground(ImageView imageView) {
        if (imageView != null) {
            Bitmap bitmap = BitmapCache.INSTANCE.getBitmap(CACHE_BG_KEY);
            if (bitmap == null) {
                bitmap = BitmapDecoder.from(getResources(), getBackgroundDrawableId())
                        .region(getBackgroundBounds()).decode();
                BitmapCache.INSTANCE.putBitmap(CACHE_BG_KEY, bitmap);
            }

            imageView.setImageBitmap(bitmap);
        }
    }

    protected abstract int getBackgroundDrawableId();

}