package ru.klesh.memecreator.gui.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import ru.klesh.memecreator.R;
import ru.klesh.memecreator.common.SoundManager;
import ru.klesh.memecreator.common.lib.Sounds;
import ru.klesh.memecreator.common.meme.MemeManager;

public class ActivityLoading extends ActivityBackgrounded {

    private boolean started = false;
    private final String TAG_START = "started";
    private final int SD_CARD_WRITE_REQUEST = 1;

    private Thread loadingThread = null;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_loading;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            this.started = savedInstanceState.getBoolean(TAG_START, false);
        }

        backgroundImageView.setOnClickListener(this);

        if (!started) {
            int check = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (check != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        SD_CARD_WRITE_REQUEST);

            } else {
                load();
            }
        }
    }

    @Override
    protected Rect getBackgroundBounds() {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, getBackgroundDrawableId(), opts);

        double coff = 0;
        if (displayHeight >= displayWidth) {
            coff = (double) opts.outHeight / displayHeight;
        } else {
            coff = (double) opts.outWidth / displayWidth;
        }

        //int scaledDW = (int) (displayWidth * coff);
        int scaledDH = (int) (displayHeight * coff);
        //int avgW = (opts.outWidth - scaledDW) / 2;
        //int avgH = (opts.outHeight - scaledDH) / 2;

        return new Rect(0, 0, opts.outWidth, scaledDH);
    }

    private void load() {

        final Activity instance = this;

        started = true;
        Toast.makeText(getBaseContext(), "Loading", Toast.LENGTH_SHORT).show();
        SoundManager.INSTANCE.load(Sounds.BUTTON_TAP);
        MemeManager.INSTANCE.load(new MemeManager.EndListener() {
            @Override
            public void onEnd(MemeManager manager, boolean success) {
                if (!success) {
                    Exception ex = manager.getLastException();
                    String msg = ex.getClass().getSimpleName() + ": " + ex.getMessage();
                    Toast.makeText(context, "Can't load memes: " + msg, Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    loadingThread = Thread.currentThread();

                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException ignored) {
                    }

                    Toast.makeText(context, "Successfully loaded", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(instance, ActivityMain.class);
                    instance.startActivityForResult(intent, 0);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case SD_CARD_WRITE_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    load();
                } else {
                    finish();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        finish();
    }

    @Override
    protected int getBackgroundDrawableId() {
        return R.drawable.loading_bg;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(TAG_START, started);
    }

    @Override
    public void onClick(View v) {
        if (this.loadingThread != null) {
            loadingThread.interrupt();
        }
    }
}
