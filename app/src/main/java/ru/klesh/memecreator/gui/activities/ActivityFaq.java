package ru.klesh.memecreator.gui.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;
import org.json.JSONTokener;

import ru.klesh.memecreator.R;
import ru.klesh.memecreator.common.Config;
import ru.klesh.memecreator.common.lib.utils.HTTP;
import ru.klesh.memecreator.common.meme.GSONLoadingException;
import ru.klesh.memecreator.gui.SmallSoundOverlayConfig;
import ru.klesh.memecreator.gui.SoundOverlayConfig;

public class ActivityFaq extends ActivityApp {

    private TextView faqView;
    private static String faqCachedText = null;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_faq;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        faqView = (TextView) findViewById(R.id.faq_text);

        if (faqView != null) {
            faqView.setText(R.string.faq_default_text);
            if (faqCachedText == null) {
                new DownloadFAQTask().execute(Config.FAQ_FILE_URL);
            } else {
                faqView.setText(faqCachedText);
            }
        }
    }

    @Override
    public Object[] getCheckableViews() {
        return new Object[]{faqView};
    }

    @Override
    public SoundOverlayConfig getSoundOverlayConfig() {
        return new SmallSoundOverlayConfig();
    }

    private class DownloadFAQTask extends AsyncTask<String, Void, String> {
        private Exception loadingEx = null;

        protected String doInBackground(String... urls) {
            try {
                JSONTokener tokener = new JSONTokener(HTTP.getHTML(urls[0]));
                JSONObject root = new JSONObject(tokener);

                if (root.has("success") && root.getBoolean("success")) {
                    if (root.has("data")) {
                        return root.getString("data");
                    } else {
                        loadingEx = new GSONLoadingException("Data is not receive!");
                    }
                } else {
                    loadingEx = new GSONLoadingException("Data is not success");
                }
            } catch (Exception e) {
                e.printStackTrace();
                loadingEx = e;
            }

            return null;
        }

        protected void onPostExecute(String result) {
            if (loadingEx != null) {
                Toast.makeText(context, "Unable to load FAQ: " + loadingEx.getMessage(), Toast.LENGTH_LONG).show();
                finish();
            } else {
                faqView.setText(result);
                faqCachedText = result;
            }
        }
    }
}
