package ru.klesh.memecreator.gui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import ru.klesh.memecreator.R;
import ru.klesh.memecreator.common.Config;
import ru.klesh.memecreator.common.SoundManager;
import ru.klesh.memecreator.common.lib.Sounds;
import ru.klesh.memecreator.common.lib.utils.ViewUtils;
import ru.klesh.memecreator.gui.SoundOverlayConfig;

public abstract class ActivityApp extends ActivityBackgrounded {

    private ImageButton soundOverlayButton = null;
    private SoundOverlayConfig soundOverlayConfig = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.soundOverlayConfig = getSoundOverlayConfig();
        this.soundOverlayButton = (ImageButton) findViewById(soundOverlayConfig.soundButtonResId);
        this.setupListeners();
        updateOverlayButton();
    }

    @Override
    protected int getBackgroundDrawableId() {
        return R.drawable.bg;
    }

    public abstract SoundOverlayConfig getSoundOverlayConfig();

    private void updateOverlayButton() {
        if (soundOverlayButton != null) {
            ViewUtils.switchImageOnView(soundOverlayButton,
                    SoundManager.INSTANCE.isMute(),
                    soundOverlayConfig.drawableOffResId,
                    soundOverlayConfig.drawableOnResId);
        }
    }

    private void setupListeners() {
        this.registerOnClickListener(soundOverlayConfig.soundButtonResId);
    }
    
    @Override
    public void onClick(View v) {
        if (soundOverlayConfig.soundButtonResId == v.getId()) {
            SoundManager.INSTANCE.setMute(!SoundManager.INSTANCE.isMute());
            Config.INSTANCE.setBoolean(Config.MUTE_PROP, SoundManager.INSTANCE.isMute());
            SoundManager.INSTANCE.playRandomly(Sounds.BUTTON_TAP);
            this.updateOverlayButton();
        }
    }

    @Override
    public int getLayoutResId() {
        return 0;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        updateOverlayButton();
    }
}
