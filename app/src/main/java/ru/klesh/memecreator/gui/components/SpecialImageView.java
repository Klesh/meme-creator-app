package ru.klesh.memecreator.gui.components;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.ImageView;

public class SpecialImageView extends ImageView {
    public static final int MAX_FREE_SPACE_HEIGHT_PERCENT_OF_SCREEN = 60;

    private int bWidth = -1;
    private int bHeight = -1;
    private float bAspectRatio;
    private boolean widthDominant = false;
    private boolean empty = true;
    private int maxHeight;

    public SpecialImageView(Context context) {
        super(context);
        init(context);
    }

    public SpecialImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        int screenHeight = getResources().getDisplayMetrics().heightPixels;
        this.maxHeight = screenHeight - (screenHeight * MAX_FREE_SPACE_HEIGHT_PERCENT_OF_SCREEN / 100);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        empty = bm == null;

        if (!empty) {
            bWidth = bm.getWidth();
            bHeight = bm.getHeight();
            widthDominant = bWidth >= bHeight;
            bAspectRatio = widthDominant ? (float) bWidth / bHeight : (float) bHeight / bWidth;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (!empty) {
            int newWidth = getMeasuredWidth();
            int newHeight;

            newHeight = (int) (newWidth / bAspectRatio);
            if (newHeight > maxHeight) {
                newHeight = maxHeight;
            }

            setMeasuredDimension(newWidth, newHeight);
        }
    }
}
