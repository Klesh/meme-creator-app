package ru.klesh.memecreator.gui.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;

import ru.klesh.memecreator.common.Config;
import ru.klesh.memecreator.common.SoundManager;

public abstract class ActivityBase extends AppCompatActivity implements View.OnClickListener {

    protected Resources resources;
    protected Context context;
    protected int displayWidth;
    protected int displayHeight;
    private Display display;

    public abstract int getLayoutResId();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void startActivity(Intent intent, Bundle options) {
        super.startActivityForResult(intent, 0, options);
    }

    private boolean layoutCheck(Object[] toCheck) {
        boolean success = true;

        for (Object o : toCheck) {
            if (o == null) {
                success = false;
                break;
            }
        }

        return success;
    }

    public Object[] getCheckableViews() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(this.getLayoutResId());

        this.resources = getResources();
        this.context = getBaseContext();
        this.display = getWindowManager().getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);
        this.displayWidth = size.x;
        this.displayHeight = size.y;

        SoundManager.INSTANCE.setContext(getBaseContext());
        Config.INSTANCE.setContext(getBaseContext());
        SoundManager.INSTANCE.setMute(Config.INSTANCE.getBoolean(Config.MUTE_PROP, false));
    }

    @Override
    protected void onStart() {
        super.onStart();
        performLayoutCheck();
    }

    protected void performLayoutCheck() {
        Object[] toCheck = getCheckableViews();
        if (toCheck != null && !layoutCheck(toCheck)) {
            throw new IllegalStateException("Layout is wrong!");
        }
    }

    protected void registerOnClickListener(int id) {
        View view = findViewById(id);
        if (view != null) {
            view.setOnClickListener(this);
        }
    }
}
