package ru.klesh.memecreator;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.nullwire.trace.ExceptionHandler;
import com.vk.sdk.VKSdk;

import ru.klesh.memecreator.common.Config;

public class MemCreatorApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        doBindServices();
    }

    void doBindServices() {
        VKSdk.initialize(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        ExceptionHandler.register(this, Config.CRASH_URL);
    }

    void doUnbindServices() {
    }
}
