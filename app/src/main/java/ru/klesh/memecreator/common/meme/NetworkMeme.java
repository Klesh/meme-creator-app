package ru.klesh.memecreator.common.meme;


public class NetworkMeme {
    private String name;
    private String desc;
    private String imgUrl;
    private String imgMd5;
    private String imgPath;

    public NetworkMeme(String name, String desc, String imgUrl, String imgMd5, String imgPath) {
        this.desc = desc;
        this.imgUrl = imgUrl;
        this.imgMd5 = imgMd5.substring(0, 32);
        this.name = name;
        this.imgPath = imgPath;
    }

    public String getImgMd5() {
        return imgMd5;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public String getImgPath() {
        return imgPath;
    }
}
