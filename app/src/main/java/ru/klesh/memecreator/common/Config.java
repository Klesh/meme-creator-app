package ru.klesh.memecreator.common;

import android.content.Context;
import android.content.SharedPreferences;

public class Config {

    public static final String CRASH_URL = "http://klesh.xyz/memcreator/crash.php";
    public static final String PROVIDER_URL = "http://klesh.xyz/memcreator/provider.php";
    public static final String MEMES_URL = PROVIDER_URL + "?type=memes";
    public static final String FAQ_FILE_URL = PROVIDER_URL + "?type=faq";

    public static final String MAIN_PREFS = "settings";
    public static final String MUTE_PROP = "mute";

    public static final Config INSTANCE = new Config();
    private Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    public SharedPreferences getPreferences(String name) {
        if (context == null) {
            throw new NotAssignedContextException();
        }

        return context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public boolean getBoolean(String key, boolean def) {
        return getPreferences(MAIN_PREFS).getBoolean(key, def);
    }

    public int getInt(String key, int def) {
        return getPreferences(MAIN_PREFS).getInt(key, def);
    }

    public String getString(String key, String def) {
        return getPreferences(MAIN_PREFS).getString(key, def);
    }

    public float getFloat(String key, float def) {
        return getPreferences(MAIN_PREFS).getFloat(key, def);
    }

    public void setBoolean(String key, boolean val) {
        getPreferences(MAIN_PREFS).edit().putBoolean(key, val).commit();
    }

    public void setInt(String key, int val) {
        getPreferences(MAIN_PREFS).edit().putInt(key, val).commit();
    }

    public void setString(String key, String val) {
        getPreferences(MAIN_PREFS).edit().putString(key, val).commit();
    }

    public void setFloat(String key, float val) {
        getPreferences(MAIN_PREFS).edit().putFloat(key, val).commit();
    }
}
