package ru.klesh.memecreator.common.lib.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTP {

    public static final int TIMEOUT_MILLIS = 60000;

    public static String getHTML(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(TIMEOUT_MILLIS);
        conn.setRequestMethod("GET");

        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();

        return result.toString();
    }

    public static boolean getSmallFile(File fileToSave, String urlToDownload) throws IOException {
        boolean ready = false;
        if (fileToSave.exists()) {
            ready = fileToSave.delete() && fileToSave.createNewFile();
        } else {
            ready = fileToSave.createNewFile();
        }

        if (ready) {

            URL url = new URL(urlToDownload);
            BufferedInputStream in = new BufferedInputStream(url.openStream());
            FileOutputStream fos = new FileOutputStream(fileToSave);
            BufferedOutputStream bout = new BufferedOutputStream(fos);

            int read;
            byte data[] = new byte[1024];

            while ((read = in.read(data, 0, 1024)) >= 0) {
                bout.write(data, 0, read);
            }

            bout.close();
            in.close();
        }

        return ready;
    }


}
