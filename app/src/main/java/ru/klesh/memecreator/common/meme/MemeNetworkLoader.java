package ru.klesh.memecreator.common.meme;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import ru.klesh.memecreator.common.lib.utils.HTTP;

public class MemeNetworkLoader {


    private String url;

    public MemeNetworkLoader(String url) {
        this.url = url;
    }

    public NetworkMeme[] download() throws Exception {
        JSONTokener tokener = new JSONTokener(HTTP.getHTML(url));
        JSONObject root = new JSONObject(tokener);
        List<NetworkMeme> memes = new ArrayList<>();

        String[] requiredEntryKeys = new String[]{
                "desc",
                "image_url",
                "image_md5",
                "image_file"
        };

        if (root.has("success") && root.getBoolean("success")) {
            if (root.has("data")) {
                JSONObject data = root.getJSONObject("data");

                Iterator it = data.keys();
                while (it.hasNext()) {
                    JSONObject entry = data.getJSONObject((String) it.next());

                    if (this.isJsonHasGroup(entry, requiredEntryKeys)) {
                        String[] desc = entry.getString("desc").split(Pattern.quote("<part>"));

                        if (desc.length == 2) {
                            memes.add(new NetworkMeme(
                                    desc[0].trim(),
                                    desc[1].trim(),
                                    entry.getString("image_url"),
                                    entry.getString("image_md5"),
                                    entry.getString("image_file")));
                        } else {
                            throw new GSONLoadingException("Desc part count is invalid!");
                        }
                    } else {
                        throw new GSONLoadingException("Data entry is corrupt!");
                    }
                }
            } else {
                throw new GSONLoadingException("Data object is not exists!");
            }
        } else {
            throw new GSONLoadingException("Data is not success!");
        }

        return memes.toArray(new NetworkMeme[memes.size()]);
    }

    private boolean isJsonHasGroup(JSONObject jsonObject, String[] keys) {
        for (String key : keys) {
            if (!jsonObject.has(key)) {
                return false;
            }
        }
        return true;
    }
}
