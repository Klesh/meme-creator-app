package ru.klesh.memecreator.common.meme;

import android.os.Looper;

import ru.klesh.memecreator.common.Config;

public class MemeManager {

    public static MemeManager INSTANCE = new MemeManager();
    private MemeNetworkLoader networkLoader;
    private Meme[] memes;
    private Exception lastException;

    public MemeManager() {
        this.networkLoader = new MemeNetworkLoader(Config.MEMES_URL);
    }

    public void load(final EndListener endListener) {
        final MemeManager self = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();

                try {
                    self.lastException = null;
                    NetworkMeme[] networkMemes = networkLoader.download();

                    if (networkMemes.length > 0) {
                        MemeCache cache = MemeCache.INSTANCE;

                        if (cache.updateCache(networkMemes)) {
                            self.memes = cache.getMemes();

                            if (self.memes.length <= 0) {
                                self.lastException = new IllegalStateException("Cached memes length <= 0");
                            }
                        } else {
                            self.lastException = new IllegalStateException("updateCache false");
                        }
                    } else {
                        self.lastException = new IllegalStateException("Network memes length <= 0");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    self.lastException = e;
                }

                endListener.onEnd(self, self.lastException == null);

                Looper.loop();
                Looper.getMainLooper().quit();
            }
        }, "Meme loading thread").start();
    }


    public Meme[] getMemes() {
        return memes;
    }

    public Exception getLastException() {
        return lastException;
    }

    public abstract static class EndListener {
        public abstract void onEnd(MemeManager manager, boolean success);
    }
}
