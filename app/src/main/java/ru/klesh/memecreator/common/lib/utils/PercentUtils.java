package ru.klesh.memecreator.common.lib.utils;

public class PercentUtils {

    public static int percentOf(int value, int percent) {
        return value * percent / 100;
    }

    public static float percentOf(float value, int percent) {
        return value * percent / 100;
    }

    public static double percentOf(double value, int percent) {
        return value * percent / 100;
    }

}
