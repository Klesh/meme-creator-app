package ru.klesh.memecreator.common.meme;

import java.io.File;

public class Meme extends NetworkMeme {
    private File imageFile;

    public Meme(File imageFile, NetworkMeme networkMeme) {
        super(networkMeme.getName(),
                networkMeme.getDesc(),
                networkMeme.getImgUrl(),
                networkMeme.getImgMd5(),
                networkMeme.getImgPath());
        this.imageFile = imageFile;
    }

    public File getImageFile() {
        return imageFile;
    }
}
