package ru.klesh.memecreator.common;

public class NotAssignedContextException extends IllegalStateException {
    public NotAssignedContextException() {
        super("Context has not been assigned!");
    }
}
