package ru.klesh.memecreator.common.meme;

import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ru.klesh.memecreator.common.lib.utils.HTTP;
import ru.klesh.memecreator.common.lib.utils.MD5;

public class MemeCache {

    public static final String CACHE_PATH = Environment.getExternalStorageDirectory() + "/MemCreator/.cache/";
    public static MemeCache INSTANCE = new MemeCache();

    private List<Meme> memeList = new ArrayList<>();
    private File cacheDir;

    private void prepareCacheDir() throws IOException {
        this.cacheDir = new File(CACHE_PATH);
        if (!cacheDir.exists()) {
            if (!cacheDir.mkdirs()) {
                throw new IOException("Can't create cache directory!");
            }
        }
    }

    public boolean updateCache(NetworkMeme[] networkMemes) throws IOException {
        this.prepareCacheDir();
        for (NetworkMeme meme : networkMemes) {
            File image = new File(cacheDir, meme.getImgPath());

            boolean needUpdate = !image.exists();
            needUpdate = needUpdate || !MD5.checkMD5(meme.getImgMd5(), image);

            if (needUpdate && !HTTP.getSmallFile(image, meme.getImgUrl())) {
                return false;
            }
            memeList.add(new Meme(image, meme));
        }
        return true;
    }

    public Meme[] getMemes() {
        return memeList.toArray(new Meme[memeList.size()]);
    }
}
