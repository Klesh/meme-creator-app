package ru.klesh.memecreator.common;

import android.content.Context;
import android.media.SoundPool;
import android.os.Build;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import ru.klesh.memecreator.common.lib.Sounds;

public class SoundManager {

    public static final SoundManager INSTANCE = new SoundManager(16);
    public static final int SOUND_LIFE_TIME_MS = 1000 * 60 * 5;
    public static final int PLAY_COUNT_LIFE_CHECK = 300;

    private Map<Integer, Sound> loadedSounds = new HashMap<>();
    private boolean mute = false;
    private Context context = null;
    private Random rand = new Random();
    private SoundPool soundPool;
    private int playCounter = PLAY_COUNT_LIFE_CHECK;
    private boolean lazyLoading = true;

    public SoundManager(int maxStreams) {
        SoundPool.Builder builder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new SoundPool.Builder();
            builder.setMaxStreams(maxStreams);
            this.soundPool = builder.build();
        } else {
            this.soundPool = new SoundPool(maxStreams, 0, 0);
        }
    }

    public void load(Sounds sounds) {
        for (int resId : sounds.getResIds()) {
            this.checkLoad(resId);
        }
    }

    public void setMute(boolean mute) {
        this.mute = mute;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public boolean isMute() {
        return mute;
    }

    public void setLazyLoading(boolean lazyLoading) {
        this.lazyLoading = lazyLoading;
    }

    public void checkLoad(int resId) {
        if (context == null) {
            throw new NotAssignedContextException();
        }

        if (lazyLoading) {
            if (!loadedSounds.containsKey(resId)) {
                Sound sound = new Sound(soundPool.load(context, resId, 1));
                loadedSounds.put(resId, sound);
            }
        } else {
            throw new RuntimeException("Sound not loaded!");
        }
    }

    private void play0(int soundId) {
        soundPool.play(soundId, 1, 1, 1, 0, 1);
        checkLifeTime();
        playCounter--;
    }

    private void checkLifeTime() {
        if (playCounter <= 0) {
            Iterator<Map.Entry<Integer, Sound>> it = loadedSounds.entrySet().iterator();

            while (it.hasNext()) {
                Sound sound = it.next().getValue();
                if (sound.isNeedUnload()) {
                    soundPool.unload(sound.getSoundId());
                    it.remove();
                }
            }

            playCounter = PLAY_COUNT_LIFE_CHECK;
        }
    }

    public void playRandomly(Sounds sounds) {
        if (!isMute()) {
            int[] resIds = sounds.getResIds();
            int resId = resIds[rand.nextInt(resIds.length)];
            this.checkLoad(resId);
            this.play0(loadedSounds.get(resId).getSoundId());
        }
    }

    public void play(Sounds sounds) {
        if (!isMute()) {
            int resId = sounds.getResIds()[0];
            this.checkLoad(resId);
            this.play0(loadedSounds.get(resId).getSoundId());
        }
    }

    private static class Sound {
        private int soundId;
        private long lastPlayTime;

        public Sound(int soundId) {
            this.soundId = soundId;
            getSoundId();
        }

        public int getSoundId() {
            lastPlayTime = System.currentTimeMillis();
            return soundId;
        }

        public boolean isNeedUnload() {
            return System.currentTimeMillis() - lastPlayTime > SOUND_LIFE_TIME_MS;
        }
    }
}
