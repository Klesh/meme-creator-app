package ru.klesh.memecreator.common;

import android.graphics.Bitmap;

import java.util.Map;
import java.util.WeakHashMap;

public class BitmapCache {

    public static BitmapCache INSTANCE = new BitmapCache();

    private Map<String, Bitmap> bitmapMap = new WeakHashMap<>();

    public void putBitmap(String name, Bitmap bitmap) {
        if (bitmap == null) {
            Bitmap prev = bitmapMap.remove(name);

            if (prev != null) {
                prev.recycle();
            }
            return;
        }

        Bitmap prev = bitmapMap.put(name, bitmap);

        if (prev != null) {
            prev.recycle();
        }
    }

    public Bitmap getBitmap(String name) {
        return bitmapMap.get(name);
    }
}
