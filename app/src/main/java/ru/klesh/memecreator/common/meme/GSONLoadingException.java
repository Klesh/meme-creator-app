package ru.klesh.memecreator.common.meme;

public class GSONLoadingException extends Exception {
    public GSONLoadingException(String message) {
        super(message);
    }
}