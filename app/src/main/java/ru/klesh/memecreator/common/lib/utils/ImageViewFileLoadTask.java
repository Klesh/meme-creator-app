package ru.klesh.memecreator.common.lib.utils;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

public class ImageViewFileLoadTask extends AsyncTask<Integer, Void, Bitmap> {
    private final WeakReference<ImageView> imageViewReference;
    private String imagePath;

    public ImageViewFileLoadTask(ImageView imageView, String imagePath) {
        imageViewReference = new WeakReference<>(imageView);
        this.imagePath = imagePath;
    }

    @Override
    protected Bitmap doInBackground(Integer... params) {
        if (params.length >= 2) {
            return BitmapUtils.decodeSampledBitmapFromFile(imagePath, params[0], params[1]);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (bitmap != null) {
            final ImageView imageView = imageViewReference.get();
            if (imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }
}