package ru.klesh.memecreator.common.lib;

import ru.klesh.memecreator.R;

public enum Sounds {
    BUTTON_TAP(
            R.raw.touch1,
            R.raw.touch2,
            R.raw.touch3,
            R.raw.touch4,
            R.raw.touch5,
            R.raw.touch6,
            R.raw.touch7,
            R.raw.touch8,
            R.raw.touch9,
            R.raw.touch10,
            R.raw.touch11
    );

    private int[] resIds;

    Sounds(int resId) {
        this.resIds = new int[]{resId};
    }

    Sounds(int... resIds) {
        if (resIds.length == 0) {
            throw new IllegalArgumentException("Resource id array can't be zero length");
        }

        this.resIds = resIds;
    }

    public int[] getResIds() {
        return resIds;
    }
}
